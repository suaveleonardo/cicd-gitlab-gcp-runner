## GPC
- Criar uma instância que é uma maquina virtual dentro do GCP. Após isso baixar o PuTTY para criar a chave SSH (public key) (forma mais facil de fazer). <br>
- Para isso abra o PuTTY Gen e clique em Generate, após isso fique fazendo movimentos com o mouse para capturar esss movimentos e então criar a chave SSH. Altere o Key comment que será seu nome de usuário pra entrar na MV posteriormente.<br>
- Copie a chave gerada e na instância (segurança e acesso) clique em adicionar item e cole a chave gerada nesse campo e precione ENTER. <br>
- No PuTTY Gen salve a private key (arquivo que vai dar acesso a instância com a chave SSH gerada anteriormente).<br> OBS: Pode ser sakvi a public key que é a chave gerada para ser utilizada em mais locais posteriormente.<br>
### Acesso a MV
- Copie o IP externo da instância criada no GCP, abra o PuTTY Configuration, cole o IP Externo em Host Name (or Ip address) port = 22. <br>
- Filtre por SSH, Auth e selecione em Private key file for authentication o arquivo salvo com a private key (campo de escolha do arquivo pode estar em subitens do Auth). <br>
- Clique em Open e Accept. No terminal aberto vai pedir login as que é o valor inserido em Key comment. Após isso tera acesso a MV.

### Instalando Docker no Debian
Adicionar os comandos abaixo. <br>
-   sudo su (adiciona como super usuário e os comando não precisa ser usados com sudo na frente). <br>
- curl - fsSL https://get.docker.com -o get-docker.sh (Vai baixar o script). <br>
- sudo sh get-docker.sh (Instalação do Docker).

### Instalando o Gitlab Runner
Primeiramente atualize a MV.<br>
- apt update <br>
- apt upgrade (Precione Y para concordar)<br>
Instale o Gitlab Runner<br>
- curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash <br>
- sudo apt-get install gitlab-runner <br>
Registre o servidor como um Runner. <br>
- gitlab-runner register <br>
- Vai pedir uma url, será https://gitlab.com/ <br>
- Vai pedir um token, basta ir em Settings e CI/CD no repositório do gitlab, após isso ir nos três pontos do Project runners  e copiar Registration token e colar no terminal. <br>
- Vai pedir tags, pode ser qualquer coisa ex desse : gcp, gitlab, linux. (Essas TAGS são utilizadas dentro dos JOBS da PIPELINE, para especificar q o JOB será utilizado nessa instância)<br>
- Vai pedir um executor, insira shell (por causa do linux)<br>

### Iniciar o gitlab-runner
- gitlab-runner start <br>
Em Settings, CI/CD vai ter um Available specific runners em funcionamento, mostrando as tags inseridas e opções e edição no runner criado. <br>
Isso permite fazer um deploy diretamente no servidor criado no GCP. 

### Adição de permissão para usuários utilizar o Docker
Da forma atual apenas o usuário root tem permissão, deve ser feito a adição do usuário GCP e o usuário Gitlab (gitlab-runner) que loga na instância GCP e executa comandos pedidos. Para adicionar permissão ao GCP e gitlab-runner execute os seguintes comandos, cada um referente a uma permissão. No terminal se estiver como root@..., insira exit e garanta que esteja com o nome de usuário da instância, que nesse caso é gcp (nome criado no Key comment da chave SSH para a instância).<br>
- sudo usermod -aG docker gcp (adiciona permissão ao GCP) <br>
- sudo usermod -aG docker gitlab-runner (adiciona permissão ao gitlab-runner) <br>

### Trocando de usuário no terminal e adicionando conta Docker Hub
Para isso passa pra usuário root para não pedir senha quando logar nas outras instâncias
- sudo su <br>
- su gitlab-runner<br>

Logar a conta do Docker para poder fazer Push e Pull de imagens pela instância GCP utilizando o usuário gitlab-runner<br>
Utilizando o usuário gitlab-runner, inserir os comandos para logar a conta Docker Hub: <br>
- docker login (inserir username e depois a senha)<br>


Repetir o processo para o usuário gcp. (Caso pessa PASSWORD quando entrar no usuário gcp basta dar exit antes, e entrar a partir do root@ que neste caso não pede a senha).

## Subindo aplicação em Docker para o server do GCP
- Criar um arquivo html básico dentro de uma pasta app <br>
- Dentro da pasta app criar o arquivo dockerfile que vai ter os comandos de execução e imagem apache necessária para fazer o deploy.<br>
- Criar na raiz do projeto o arquivo .gitlab-ci.yml que vai ter as configurações dos JOBS da pipeline (1 job de criação da imagem e 1 job de deploy da imagem). <br>

OBS:
- Teremos 2 stages, um de construção da imagem e outro que faz o deploy da imagem. <br>
- O primeiro JOB é responsavem por criar a imagem, referenciando onde será executado pela TAG gpc. O script vai ter um comando de crianção da imagem, sendo pego tudo que esta dentro da pasta app e construindo uma imagem com o nome leonardosuave/gcp-runner que posteriormente sera encontrado no Docker Hub da conta vinculada. O segundo script vai fazer o push dessa imagem para o Docker Hub. <br>
- No segundo JOB será executado a imagem armazenada no Docker Hub, criando um container de nome webserver na porta 80. <br>